#include "newton.h"
#include <cmath>
using std::abs;

double step(Func f, Func df, const double x) noexcept
{
    return x - f(x)/df(x);
}

double findRootNewton(Func f, Func df, const double x, const double tolerance) noexcept
{
    return abs(f(x)) < tolerance ? x : findRootNewton(f, df, step(f, df, x), tolerance);
}

double findRootBisect(Func f, const double lower, const double upper, const double tolerance) noexcept
{
    const auto x = lower + (upper - lower)/2.0;
    const auto val = f(x);
    if(abs(val) < tolerance) return x;
    return val < 0.0 ? findRootBisect(f, x, upper, tolerance) : findRootBisect(f, lower, x, tolerance);
}

double invertNewton(Func f, Func df, const double y, const double x0, const double tolerance) noexcept
{
    const auto objective = [=](const double x){return f(x) - y;};
    return  findRootNewton(objective, df, x0, tolerance);
}

double invertBisect(Func f, const double y, const double upper, const double lower, const double tolerance) noexcept
{
    const auto objective = [=](const double x){return f(x) - y;};
    return  findRootBisect(objective, upper, lower, tolerance);
}
