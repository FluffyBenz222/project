#include "fileIO.h"
#include "pavement.h"
using std::ifstream;
using std::shared_ptr;
using json = nlohmann::json;
using std::make_shared;
using std::vector;

shared_ptr<AsphaltConstants> getParams(const char* configFileName)
{
    ifstream ifs(configFileName);
    json j;
    ifs >> j;
    return make_shared<AsphaltConstants>(j["Eaf"], j["Af"], j["R"], j["M"], j["logG0"], j["Eac"], j["Ac"], j["nReplicates"]);
}

vector<double> getData(const char* dataFileName)
{
    ifstream ifs(dataFileName);
    json j;
    ifs >> j;
    vector<double> dataVec;
    for(const auto x :  j["data"]) dataVec.push_back(x);
    return dataVec; 
}

void toFile(const json& data, const char* fileName)
{
    std::ofstream file(fileName);
    file << data;
}

void writeData(const vector<double> values, const vector<double> times, const char* fileName)
{
    json retVal;
    retVal["values"] = values;
    retVal["times"] = times;
    toFile(retVal, fileName);
}

