#include "pavement.h"
#include <cmath>
#include <iostream>
#include "fileIO.h"

using std::shared_ptr;
using std::vector;
const auto hour = 1.0/24.0;
const double upper = 365*2000000.0;
const double lower = 0.0;

#define my_tolerance 1e-12

double getKf(const double temp, const AsphaltConstants& p)
{
    return p.m_Af*exp(-p.m_Eaf/(p.m_R*temp));
}

double getKc(const double temp, const AsphaltConstants& p)
{
    return p.m_Ac*exp(-p.m_Eac/(p.m_R*temp));
}

double gStar(const double time, const double temp, const AsphaltConstants& p)
{
    const auto kf = getKf(temp, p);
    const auto kc = getKc(temp, p);
    return p.m_logG0 + p.m_M* ((1 - kc/kf)*(1 - exp(-kf*time)) + kc*time);
}

double gStarDerivitive(const double time, const double temp, const AsphaltConstants& p)
{
    const auto kf = getKf(temp, p);
    const auto kc = getKc(temp, p);
    return p.m_M*((1 - kc/kf)*(kf)*exp(-kf*time) + p.m_M*kc);
}

void processSingleIteration(
    const shared_ptr<AsphaltConstants>& params,
    const vector<double>& temperatures,
    vector<double>& times,
    vector<double>& values,
	const int startIndex)
{
    for(auto i = startIndex; i < temperatures.size(); i++)
    {
		const auto currentTemp = temperatures[i];
        const auto f = [currentTemp, params](const double t){return gStar(t, currentTemp, *params);};
        const auto inverse = invertBisect(f, values.back(), lower, upper, my_tolerance);
        const auto time = inverse + hour;
        times.push_back(time);
        values.push_back(f(time));
    }
}

void run(const shared_ptr<AsphaltConstants>& params, const vector<double>& temperatures, const char* outfile)
{
    vector<double> times({1.0*hour});
    vector<double> values({gStar(1.0*hour, temperatures[0], *params)});
    for(auto i = 0; i < params->m_nReplicates; i++)
    {
        std::cout << "processing year " << i + 1 << std::endl;
        processSingleIteration(params, temperatures, times,  values, i == 0 ? 1 : 0);
    }
    writeData(values, times, outfile);
}
