
import json
import numpy as np
import matplotlib.pyplot as plt
def getConfig(fileName):
    json_data = open('config.json').read()
    return json.loads(json_data)


def getF(temp, time):
    logG0 = config['logG0']
    M = config['M']
    Eac = config['Eac']
    Eaf = config['Eaf']
    Ac = config['Ac']
    Af = config['Af']
    R = config['R']
    Kc = Ac*np.exp(-Eac/(R*temp))
    Kf = Af*np.exp(-Eaf/(R*temp))
    return logG0 + M*(1 - Kc/Kf)*(1-np.exp(-Kf*time)) + Kc*M*time

if __name__ == "__main__":
    configFileName = 'config.json'
    config = getConfig(configFileName)
    temp = 270
    times = np.arange(0, 100, 0.01)
    helper = lambda x : getF(temp, x)
    vals = map(helper, times)
    plt.plot(times, vals)
    plt.show()
